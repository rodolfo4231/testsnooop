﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SnoopTestClientes.Models
{
    public class Clientes
    {
        [Key]
        public int CLienteID { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "El nombre debe de tener de 3 a 50 caracteres")]

        public string Nombre { get; set; }
        [StringLength(50, MinimumLength = 3, ErrorMessage = "El Apellido debe de tener de 3 a 50 caracteres")]
        public string Apellido { get; set; }



    }
}
