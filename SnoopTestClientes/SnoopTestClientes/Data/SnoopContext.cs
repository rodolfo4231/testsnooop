﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace SnoopTestClientes.Models
{
    public class SnoopContext : DbContext
    {
        public SnoopContext (DbContextOptions<SnoopContext> options)
            : base(options)
        {
        }

        public DbSet<SnoopTestClientes.Models.Clientes> Clientes { get; set; }
    }
}
