﻿using SnoopTestClientes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SnoopTestClientes.Data
{
    public class DbInitializer
    {
        public static void Initialize(SnoopContext context)
        {
            context.Database.EnsureCreated();

            //Buscar si existen registros en la tabla categoría
            if (context.Clientes.Any())
            {
                return;
            }
            var categorias = new Clientes[]
            {
                new Clientes{Nombre="Rodolfo",Apellido="ramos"},
                new Clientes{Nombre="Jose",Apellido="Rodriguez"}
            };

            foreach (Clientes c in categorias)
            {
                context.Clientes.Add(c);
            }
            context.SaveChanges();

        }
    }
}
